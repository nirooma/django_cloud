import React from "react";
import ReactDOM from "react-dom";
import axios from 'axios';

axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.withCredentials = true;



class App extends React.Component {
   login() {
      axios.post('http://localhost:8000/login/', {
          username: "nirooma",
          password: "12345"
      }).then(response => {
            console.log(response)
        })
        .catch(error => {
            console.log(error.response)
        });
  }

  async test() {
    return await axios.post('http://localhost:8000/check_helth/').then(res => console.log(res))
  }
  async logout() {
    return await axios.post('http://localhost:8000/check_logout/').then(res => alert(res.data))
  }
  render() {
    return (
      <div>
          <h1>Hello React.Js App</h1>
        <button onClick={this.login}>Login now</button>
        <button  onClick={this.test}>Check for ping</button>
        <button  onClick={this.logout}>Logout</button>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
