from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_403_FORBIDDEN
from rest_framework.permissions import AllowAny
from cloud.models import User
from rest.serializers import UserSerializer
import logging

logger = logging.getLogger(__name__)


class LoginViewSet(ModelViewSet):
    permission_classes = [AllowAny]
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def create(self, request, *args, **kwargs):
        from utils.loginlogoutUtils import login_user

        data     = request.data
        username = data.get("username")
        password = data.get("password")

        logger.debug(f'LoginViewSet:: get user data - {data}')


        user = login_user(request=request, username=username, password=password)

        if user is not None:
            return Response(UserSerializer(user).data, status=HTTP_200_OK)

        return Response('Invalid Username/Password', status=HTTP_403_FORBIDDEN)


