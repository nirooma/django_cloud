from rest_framework import serializers
from cloud.models import User


class UserSerializer(serializers.ModelSerializer):
    user_allowed_to_reset_password = serializers.SerializerMethodField()

    class Meta:
        fields = '__all__'
        model = User

    def get_user_allowed_to_reset_password(self, obj):

        user_allowed_to_reset_password = True if obj.user_token is not None else False
        return user_allowed_to_reset_password
