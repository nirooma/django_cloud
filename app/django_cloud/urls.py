from django.contrib import admin
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest.views.LoginLogoutViewSet import LoginViewSet

router = DefaultRouter()

router.register(r'login', LoginViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += router.urls