from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    phone                   = models.CharField(max_length=30, default=None, null=True)
    user_token              = models.CharField(max_length=50, default=None, null=True)
    token_reset_datetime    = models.DateTimeField(default=None, null=True)
    options                 = models.JSONField(null=True)

    def __str__(self):
        return self.username

