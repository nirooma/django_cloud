# Generated by Django 3.1 on 2020-08-23 16:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cloud', '0005_auto_20200823_1620'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='phone',
            field=models.CharField(default=None, max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='user_token',
            field=models.CharField(default=None, max_length=50, null=True),
        ),
    ]
