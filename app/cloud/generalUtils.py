import secrets

def random_secret_token():
    return secrets.token_hex()