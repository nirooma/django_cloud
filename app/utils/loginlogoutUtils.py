import nexmo
from django.contrib.auth import authenticate, login


def login_user(request, username, password):
    user = authenticate(request=request, username=username, password=password)
    if user is not None:
        login(request=request, user=user)
        return user
    return None